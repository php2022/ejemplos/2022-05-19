<?php
    require "./header.php";
?>
<div class="container-fluid">
    <div class="row row-cols-1 row-cols-md-3 g-3">
        <div class="col-md-3">
            <div class="card h-100">
                <img src="imgs/5.jpg" class="card-img-top" data-toggle="modal" data-target="#foto5">
                <div class="card-body">
                    <h5 class="card-title">Foto 5</h5>
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">12 Enero 2022</small>
                </div>
            </div>
        </div>
        
        <div id="foto5" class="modal fade">
            <div class="modal-dialog">
                <div class="model-content">
                    <img src="./imgs/5.jpg" class="modal-lg" data-dismiss="modal">
                </div>
            </div>
        </div>
        
        <div class="col-md-3">
            <div class="card h-100">
                <img src="imgs/6.jpg" class="card-img-top" data-toggle="modal" data-target="#foto6">
                <div class="card-body">
                    <h5 class="card-title">Foto 6</h5>
                    <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">12 Enero 2022</small>
                </div>
            </div>
        </div>
        
        <div id="foto6" class="modal fade">
            <div class="modal-dialog">
                <div class="model-content">
                    <img src="./imgs/6.jpg" class="modal-lg" data-dismiss="modal">
                </div>
            </div>
        </div>
        
        <div class="col-md-3">
            <div class="card h-100">
                <img src="imgs/7.jpg" class="card-img-top" data-toggle="modal" data-target="#foto7">
                <div class="card-body">
                    <h5 class="card-title">Foto 7</h5>
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">12 Enero 2022</small>
                </div>
            </div>
        </div>
        
        <div id="foto7" class="modal fade">
            <div class="modal-dialog">
                <div class="model-content">
                    <img src="./imgs/7.jpg" class="modal-lg" data-dismiss="modal">
                </div>
            </div>
        </div>
        
        <div class="col-md-3">
            <div class="card h-100">
                <img src="imgs/8.jpg" class="card-img-top" data-toggle="modal" data-target="#foto8">
                <div class="card-body">
                    <h5 class="card-title">Foto 8</h5>
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">12 Enero 2022</small>
                </div>
            </div>
        </div>
        
        <div id="foto8" class="modal fade">
            <div class="modal-dialog">
                <div class="model-content">
                    <img src="./imgs/8.jpg" class="modal-lg" data-dismiss="modal">
                </div>
            </div>
        </div>
        
    </div>
</div>
<?php
    require "footer.php";
