<?php
require "./header.php";
?>
<?php
if (isset($_GET["ejercicio3suma"])) {
    $numeros = $_GET["numeros"];
    $suma = 0;
    $resto = explode(";", $numeros[2]);
    unset($numeros[2]);
    $numeros = array_merge($numeros, $resto);
    foreach ($numeros as $numero) {
        $suma += $numero;
    }
} else if (isset($_GET["ejercicio3producto"])) {
    $numeros = $_GET["numeros"];
    $producto = 1;
    $resto = explode(";", $numeros[2]);
    unset($numeros[2]);
    $numeros = array_merge($numeros, $resto);
    foreach ($numeros as $numero) {
        $producto *= $numero;
    }
} else if (isset($_GET["ejercicio3"])) {
    $numeros = $_GET["numeros"];
    $suma = 0;
    $producto = 1;
    $resto = explode(";", $numeros[2]);
    unset($numeros[2]);
    $numeros = array_merge($numeros, $resto);
    foreach ($numeros as $numero) {
        $suma += $numero;
        $producto *= $numero;
    }
} else {
    ?>
    <form>
        <div class="container-fluid">
            <div class="row m-3">
                <label for="num1" class="col-sm-2 col-form-label">Número 1: </label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" id="num1" name="numeros[]">
                </div>
            </div>
            <div class="row m-3">
                <label for="num2" class="col-sm-2 col-form-label">Número 2: </label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" id="num2" name="numeros[]">
                </div>
            </div>
            <div class="row m-3">
                <label for="nums" class="col-sm-2 col-form-label">Números: </label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" id="nums" name="numeros[]"required>
                </div>
            </div>

            <div class="row">
                <div class="col-2">
                    <button type="submit" name="ejercicio3suma" class="btn btn-primary m-3">Suma</button>
                </div>

                <div class="col-2">
                    <button type="submit" name="ejercicio3" class="btn btn-primary m-3">Todo</button>
                </div>

                <div class="col-2">
                    <button type="submit" name="ejercicio3producto" class="btn btn-primary m-3">Producto</button>
                </div>
            </div>
        </div>

    </form>
    <?php
}
if (isset($suma)) {
    ?>
    <div class="container-fluid">
        <div class="row">
            <div class="card col-5 p-0 m-3">
                <div class="card-header text-center">
                    Suma
                </div>
                <div class="card-body text-center">
                    <p class="card-text"><?= $suma ?></p>
                </div>
            </div>
            <div class="card col-5 p-0 m-3">
                <div class="card-header text-center">
                    Números introducidos
                </div>
                <div class="card-body text-center">
                    <?php
                    foreach ($numeros as $valor) {
                        echo "<p class=\"card-text\">$valor</p>";
                    }
                    ?>
                </div>
            </div>
        </div>   
    </div>
    <?php
}
if (isset($producto)) {
    ?>
    <div class="container-fluid">
        <div class="row">
            <div class="card col-5 p-0 m-3">
                <div class="card-header text-center">
                    Producto
                </div>
                <div class="card-body text-center">
                    <p class="card-text"><?= $producto ?></p>
                </div>
            </div>
            <div class="card col-5 p-0 m-3">
                <div class="card-header text-center">
                    Números introducidos
                </div>
                <div class="card-body text-center">
                    <?php
                    foreach ($numeros as $valor) {
                        echo "<p class=\"card-text\">$valor</p>";
                    }
                    ?>
                </div>
            </div>
        </div>   
    </div>
    <?php
}
?>
<?php
require "footer.php";
