<?php
require "./header.php";
?>
<?php
        if(isset($_GET["boton"])) {
            $nombre=$_GET["nombre"];
            $apellidos=$_GET["apellidos"];
        ?>
        <div class="container-fluid">
            <div class="row">
                <div class="card col-5 p-0 m-3">
                    <div class="card-header text-center">
                        Nombre
                    </div>
                    <div class="card-body text-center">
                        <p class="card-text"><?=$nombre?></p>
                    </div>
                </div>
                <div class="card col-5 p-0 m-3">
                    <div class="card-header text-center">
                        Apellidos
                    </div>
                    <div class="card-body text-center">
                        <p class="card-text"><?=$apellidos?></p>
                    </div>
                </div>
            </div>   
        </div>
            
        <?php
        }else{
        ?>
            <form>
                <div class="row m-3">
                    <label for="nombre" class="col-sm-2 col-form-label">Nombre: </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nombre" name="nombre">
                    </div>
                </div>
                <div class="row m-3">
                    <label for="apellidos" class="col-sm-2 col-form-label">Apellidos: </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="apellidos" name="apellidos">
                    </div>
                </div>

                <button type="submit" name="boton" class="btn btn-primary m-3">Enviar</button>
            </form>
        <?php
        }
        ?>

<?php
    require "footer.php";