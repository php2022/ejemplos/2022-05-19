<?php
    require "./header.php";
    //require 'libreria.php';
?>
        <?php
        if(isset($_GET["boton"])) {
            $mail=$_GET["mail"];
            $contrasena=$_GET["contrasena"];
            $mes=$_GET["mes"];
            $ciudad=$_GET["ciudad"];
        ?>
        <div class="container-fluid">
            <div class="row">
                <!--EMAIL-->
                <div class="card col-5 p-0 m-3">
                    <div class="card-header text-center">
                        Email
                    </div>
                    <div class="card-body text-center">
                        <p class="card-text"><?=$mail?></p>
                    </div>
                </div>
                <!--CONTRASEÑA-->
                <div class="card col-5 p-0 m-3">
                    <div class="card-header text-center">
                        Password
                    </div>
                    <div class="card-body text-center">
                        <p class="card-text"><?=$contrasena?></p>
                    </div>
                </div>
                <!--MES ACCESO-->
                <div class="card col-5 p-0 m-3">
                    <div class="card-header text-center">
                        Mes de acceso
                    </div>
                    <div class="card-body text-center">
                        <p class="card-text"><?=$mes?></p>
                    </div>
                </div>
                <!--FORMAS ACCESO-->
                <div class="card col-5 p-0 m-3">
                    <div class="card-header text-center">
                        Formas de acceso
                    </div>
                    <div class="card-body text-center">
                        <?php
                        if(!isset($_GET["formas"])){
                            echo "<p class=\"card-text\">No has seleccionado ninguna forma de acceso</p>";
                        }else{
                            foreach($_GET["formas"] as $valor){
                                echo "<p class=\"card-text\">$valor</p>";
                            }
                        }
                        ?>
                    </div>
                </div>
                <!--CIUDAD-->
                <div class="card col-5 p-0 m-3">
                    <div class="card-header text-center">
                        Ciudad
                    </div>
                    <div class="card-body text-center">
                        <p class="card-text"><?=$ciudad?></p>
                    </div>
                </div>
                <!--NAVEGADORES UTILIZADOS-->
                <div class="card col-5 p-0 m-3">
                    <div class="card-header text-center">
                        Navegadores utilizados
                    </div>
                    <div class="card-body text-center">
                        <?php
                        if(!isset($_GET["navegadores"])){
                            echo "<p class=\"card-text\">No has seleccionado ningun navegador</p>";
                        }else{
                            foreach($_GET["navegadores"] as $value){
                                echo "<p class=\"card-text\">$value</p>";
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>   
        </div>
        <?php
        }else{
        ?>
        <form>
            <!--EMAIL-->
            <div class="row">
                <div class="m-3 col-2">
                    <label for="mail" class="form-label">Email</label>
                </div>
                <div class="m-3 col-8">
                    <input type="email" class="form-control" id="mail" name="mail" required>
                </div>
            </div>
            <!--CONTRASEÑA-->
            <div class="row">
                <div class="m-3 col-2">
                    <label for="contrasena" class="form-label">Password</label>
                </div>
                <div class="m-3 col-8">
                    <input type="password" class="form-control" id="contrasena" name="contrasena" required>
                </div>
            </div>
            <!--MES ACCESO-->
            <div class="row">
                <div class="m-3 col-2">
                    <label for="mail" class="form-label">Mes de acceso</label>
                </div>
                <div class="m-3 col-8">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="mes" value="Enero" id="enero">
                        <label class="form-check-label" for="enero">Enero</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="mes" value="Febrero" id="febrero">
                        <label class="form-check-label" for="febrero">Febrero</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="mes" value="Marzo" id="marzo">
                        <label class="form-check-label" for="marzo">Marzo</label>
                    </div>
                </div>
            </div>
            <!--FORMAS ACCESO-->
            <div class="row">
                <div class="m-3 col-2">
                    <label for="mail" class="form-label">Formas de acceso</label>
                </div>
                <div class="m-3 col-8">
                    <div class="form-check">
                        <input class="form-check-input" name="formas[]" type="checkbox" value="Telefono" id="tlf">
                        <label class="form-check-label" for="tlf">Teléfono</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" name="formas[]" type="checkbox" value="Ordenador" id="orde">
                        <label class="form-check-label" for="orde">Ordenador</label>
                    </div>
                </div>
            </div>
            <!--CIUDAD-->
            <div class="row">
                <div class="m-3 col-2">
                    <label for="mail" class="form-label">Ciudad</label>
                </div>
                <div class="m-3 col-8">
                    <select class="form-select" aria-label="Default select example" name="ciudad" id="ciudad" required="">
                        <option disabled="" selected value="">Selecciona ciudad</option>
                        <option value="Santander">Santander</option>
                        <option value="Madrid">Madrid</option>
                        <option value="Sevilla">Sevilla</option>
                    </select>
                </div>
            </div>
            <!--NAVEGADORES UTILIZADOS-->
            <div class="row">
                <div class="m-3 col-2">
                    <label for="mail" class="form-label">Navegadores utilizados</label>
                </div>
                <div class="m-3 col-8">
                    <select class="form-select" multiple aria-label="multiple select example" name="navegadores[]" id="navegadores">
                        <option disabled="" selected value="">Selecciona navegadores</option>
                        <option value="Google">Google</option>
                        <option value="Edge">Edge</option>
                        <option value="Safari">Safari</option>
                    </select>
                </div>
            </div>
            <button type="submit" name="boton" class="btn btn-primary m-3">Enviar</button>
        </form>
        <?php
        }
        ?>
<?php
    require "footer.php";
