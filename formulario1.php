<div class="container-fluid">
    <div class="row">
        <div class="card col-5 p-0 m-3">
            <div class="card-header text-center">
                Nombre
            </div>
            <div class="card-body text-center">
                <p class="card-text"><?= $nombre ?></p>
            </div>
        </div>
        <div class="card col-5 p-0 m-3">
            <div class="card-header text-center">
                Apellidos
            </div>
            <div class="card-body text-center">
                <p class="card-text"><?= $apellidos ?></p>
            </div>
        </div>
    </div>   
</div>

