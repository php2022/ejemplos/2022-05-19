<?php
function resultados() {
    ?>
    <div class="container-fluid">
        <div class="row">
            <div class="card col-5 p-0 m-3">
                <div class="card-header text-center">
                    Nombre
                </div>
                <div class="card-body text-center">
                    <p class="card-text"><?= $nombre ?></p>
                </div>
            </div>
            <div class="card col-5 p-0 m-3">
                <div class="card-header text-center">
                    Apellidos
                </div>
                <div class="card-body text-center">
                    <p class="card-text"><?= $apellidos ?></p>
                </div>
            </div>
        </div>   
    </div>
<?php
}

/**
 * 
 * @param string $ejercicio formulario que quiero que muestre
 * @param bool $validar si quiero que realice la validacion de campos
 */
function formularios($ejercicio,$validar=true){
    switch($ejercicio){
        case 'ejercicio1':
            require 'formulario1.php';
            break;
        case 'ejercicio2':
            require 'formulario2.php';
            break;
        case 'ejercicio3':
            require 'formulario3.php';
            break;
    }
}
