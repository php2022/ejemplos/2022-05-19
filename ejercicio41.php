<?php
require "./header.php";
?>
<div class="container-fluid">
    <div class="row row-cols-1 row-cols-md-3 g-3">
        <div class="col-md-3">
            <div class="card h-100">
                <img src="imgs/1.jpg" class="card-img-top" data-toggle="modal" data-target="#foto1">
                <div class="card-body">
                    <h5 class="card-title">Foto 1</h5>
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">12 Enero 2022</small>
                </div>
            </div>
        </div>

        <div id="foto1" class="modal fade">
            <div class="modal-dialog">
                <div class="model-content">
                    <img src="./imgs/1.jpg" class="modal-lg" data-dismiss="modal">
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card h-100">
                <img src="imgs/2.jpg" class="card-img-top" data-toggle="modal" data-target="#foto2">
                <div class="card-body">
                    <h5 class="card-title">Foto 2</h5>
                    <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">12 Enero 2022</small>
                </div>
            </div>
        </div>

        <div id="foto2" class="modal fade">
            <div class="modal-dialog">
                <div class="model-content">
                    <img src="./imgs/2.jpg" class="modal-lg" data-dismiss="modal">
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card h-100">
                <img src="imgs/3.jpg" class="card-img-top" data-toggle="modal" data-target="#foto3">
                <div class="card-body">
                    <h5 class="card-title">Foto 3</h5>
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">12 Enero 2022</small>
                </div>
            </div>
        </div>

        <div id="foto3" class="modal fade">
            <div class="modal-dialog">
                <div class="model-content">
                    <img src="./imgs/3.jpg" class="modal-lg" data-dismiss="modal">
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card h-100">
                <img src="imgs/4.jpg" class="card-img-top" data-toggle="modal" data-target="#foto4">
                <div class="card-body">
                    <h5 class="card-title">Foto 4</h5>
                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                </div>
                <div class="card-footer">
                    <small class="text-muted">12 Enero 2022</small>
                </div>
            </div>
        </div>

        <div id="foto4" class="modal fade">
            <div class="modal-dialog">
                <div class="model-content">
                    <img src="./imgs/4.jpg" class="modal-lg" data-dismiss="modal">
                </div>
            </div>
        </div>

    </div>
</div>

<?php
require "footer.php";
